'''
Основная точка входа в программу
'''
import sqlite3
import sys
from src.start_window.start_window.start_window import StartWindow
from src.start_window.create_group.create_group_dialog import CreateGroupDialog
from PyQt6.QtWidgets import QApplication
from src.events.EventBus import EventBus
from src.start_window.table_with_group_list import TableGroup
from src.start_window.create_group.group_name_converter import GroupNameConverter
from src.data_base.db_requests import get_teacher_groups, get_teacher_id, get_norms_info, insert_norm
from src.parce_excel.parce_normative import get_all_exercise
from src.table.table_window import TableWindow
import logging
from src.data_base.db_utils import open_db
from src.helpers_dialogs.teacher_not_exist_in_db import TeacherNotExistDialog
import weakref


class App:
    def __init__(self):
        self.on_enable()
        self.start_window = StartWindow()
        self.teacher_data = None
        self.create_group_window = None
        self.table_window = None
        self.table_group = None
        self.prev_widget = None
        self.connection = None
        self.cursor = None

    def on_enable(self):
        EventBus().on_create_group_dialog.add_listener(self._create_group_dialog)
        EventBus().on_show_group_list.add_listener(self.show_group_list)
        EventBus().on_open_table.add_listener(self.open_table)
        EventBus().on_teacher_not_exist_yes.add_listener(self._teacher_not_exist_yes)

    def _teacher_not_exist_yes(self, teacher_data: list):
        self.teacher_data = teacher_data
        CreateGroupDialog(self.connection, teacher_data)

    def _create_group_dialog(self, path: str, teacher_data: list):
        self.connection, self.cursor, res = open_db(path, teacher_data)

        self.teacher_data = teacher_data
        CreateGroupDialog(self.connection, teacher_data)

    def show_group_list(self, path: str, teacher_data: list):
        self.connection, self.cursor, res = open_db(path, teacher_data)
        self.teacher_data = teacher_data
        if not res:
            TeacherNotExistDialog(teacher_data, self.cursor, path)
            return
        id = get_teacher_id(teacher_data, self.cursor)
        if id == -1:
            return
        groups_db = get_teacher_groups(teacher_data, self.cursor)
        groups_base = [GroupNameConverter.db_to_group(x) for x in groups_db]
        self.table_group = TableGroup(groups=groups_base)
        self.table_group.btn_new.clicked.connect(lambda _: self._create_group_dialog(path, teacher_data))

        self.start_window.switch_widget(self.table_group)

    def open_table(self, group_name: str):
        self._update_norms()
        self.table_window = TableWindow(current_group=group_name,
                                        connection=self.connection,
                                        teacher_data=self.teacher_data)
        self.start_window.switch_widget(self.table_window.ui)

    def _update_norms(self):
        name_ex, num_ex = get_all_exercise()
        num_ex = [int(x) for x in num_ex]
        name_db, num_db = get_norms_info(self.cursor)
        while len(num_db) < len(num_ex):
            num_db.append(-1)
        for i, num in enumerate(num_ex):
            if num not in num_db:
                insert_norm(name_ex[i], num_ex[i], self.cursor)
        self.connection.commit()

    def run(self):
        self.start_window.show()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    app = QApplication(sys.argv)

    my_app = App()
    my_app.run()

    sys.exit(app.exec())
