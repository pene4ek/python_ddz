import logging
from PyQt6.QtWidgets import QTableWidgetItem
from PyQt6.QtCore import Qt
from src.data_base.db_requests import get_teacher_groups, get_group_info, get_stud_count, insert_stud, update_stud_name, \
    get_norms_info, insert_result, check_res_db, get_exercises, get_results
from src.start_window.create_group.group_name_converter import GroupNameConverter
from src.parce_excel.parce_normative import get_user_score


class TableDbHandler:
    def __init__(self, table):
        self.table = table
        self.exercises = get_norms_info(self.table.cursor)[0]

        self.load_from_db()

    def load_from_db(self):
        self.groups_db = get_teacher_groups(self.table.teacher_data, self.table.cursor)
        self.groups_base = [GroupNameConverter.db_to_group(x) for x in self.groups_db]

    def _load_exercises(self, term: int):
        group_names = get_exercises(term, self.table.current_group, self.table.cursor)
        for name in group_names:
            self.table.add_triple_column(name)
        self.table.add_total_column()
        self._load_results()

    def _load_results(self):
        results = get_results(self.table.terms[self.table.ui.term_combobox.currentIndex()], self.table.current_group,
                              self.table.cursor)  # res, att, st_id, norm_num
        for result in results:
            column = self.table.num_norms.index(str(result[3])) * 3 + result[1]
            row = self.table.stud_id.index(result[2]) + 1
            res = result[0]
            if res == int(res):
                res = int(res)
            item = QTableWidgetItem(str(res))
            item.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
            self.table.ui.table.setItem(row, column, item)

    def save_table(self):
        self._save_students()
        self._save_results()
        logging.debug('saved')

    def switch_group_data(self, group_index: int):
        '''
        меняет содержимое таблицы
        '''
        self.table.reset_table_data()
        self.table.remove_all_column()

        self.table.stud_names, self.table.stud_id = get_group_info(self.groups_db[self.table.current_index_group], self.table.cursor)
        self.table.ui.table.setRowCount(len(self.table.stud_names) + 1)

        for i, student in enumerate(self.table.stud_names):
            self.table.ui.table.setItem(i + 1, 0, QTableWidgetItem(str(student)))
            self.table.ui.table.setVerticalHeaderItem(i + 1, QTableWidgetItem(str(i + 1)))

        self._load_exercises(self.table.terms[self.table.ui.term_combobox.currentIndex()])
        self.table.add_last_row()

    def _save_results(self):
        for c in range(self.table.added_exercises):
            col_norm = c * 3 + 1
            exercise_name = self.table.name_norms[c]
            norm_number = self.table.num_norms[self.table.name_norms.index(exercise_name)]
            for r in range(1, self.table.ui.table.rowCount()):  # вычесть 1 после работы санька
                for attempt_ind in range(0, 3):
                    attempt_col = col_norm + attempt_ind
                    result = self.table.ui.table.item(r, attempt_col).text()
                    if result == '':
                        continue
                    result_score = get_user_score(exercise_name, result)
                    attempt_num = attempt_ind + 1
                    curr_stud_id = self.table.stud_id[r - 1]
                    if check_res_db(term=self.table.terms[self.table.ui.term_combobox.currentIndex()],
                                    norm_number=norm_number,
                                    stud_id=curr_stud_id,
                                    attempt=attempt_num,
                                    cursor=self.table.cursor):
                        continue
                    insert_result(term=self.table.terms[self.table.ui.term_combobox.currentIndex()],
                                  result=result,
                                  result_score=result_score,
                                  attempt=attempt_num,
                                  student_id=curr_stud_id,
                                  cursor=self.table.cursor,
                                  norm_number=norm_number)
                    self.table.connection.commit()

    def _save_students(self):
        logging.debug(f'{self.table.stud_id}')
        self._check_exist_stud()
        self._save_new_stud()

    def _check_exist_stud(self):
        '''
        проверка наличия имени в бд, если изменено - изменяет в бд
        '''
        edited = False
        for i, name in enumerate(self.table.stud_names):
            stud_name_table = self.table.ui.table.item(1 + i, 0).text()
            if name != stud_name_table:
                update_stud_name(stud_name_table.split(), self.table.stud_id[i], self.table.cursor)
                edited = True
        if edited:
            self.table.connection.commit()

    def _save_new_stud(self):
        '''
        добавление новых слушателей в бд
        '''
        row_count = self.table.ui.table.rowCount()
        if row_count > len(self.table.stud_id):
            count = get_stud_count(self.table.current_group, self.table.cursor)
            id_offset = 1
            for i in range(len(self.table.stud_id), row_count - 2):
                stud_data = self.table.ui.table.item(i + 1, 0).text().split()
                self.table.stud_id.append(count + id_offset)
                insert_stud(stud_data=stud_data, group_num=self.table.current_group, stud_id=(count + id_offset),
                            cursor=self.table.cursor)
                id_offset += 1
            self.table.connection.commit()
