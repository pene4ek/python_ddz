"""
Модуль для работы с таблицей
"""

from PyQt6.QtWidgets import (
    QWidget,
    QPushButton,
    QTableWidget,
    QVBoxLayout,
    QHBoxLayout,
    QComboBox,
    QHeaderView,
    QSizePolicy,
    QLineEdit,
    QLabel
)
from PyQt6.QtCore import Qt
from src.widget_styles import *


class TableWindowUI(QWidget):
    def __init__(self, exercises: list, groups: list):
        """
        Инициализация

        """
        super().__init__()
        self.groups = groups
        self.exercises = exercises
        self._init_ui()
        self.setFocus()

    def _init_ui(self):
        self.setWindowTitle("Учет успеваемости")
        self.setMinimumSize(WINDOW_WIDTH, WINDOW_HEIGHT)

        self._init_header()
        self._init_cell_double()
        self._init_group_box(self.groups)
        self._init_create_group_btn()
        self._init_table()
        self._init_footer()

        self._add_widgets()
        self.setLayout(self.main_layout)

    def _add_widgets(self):
        self.main_layout = QVBoxLayout()
        self.main_layout.addLayout(self.top_layout)
        self.main_layout.addWidget(self.table)
        self.main_layout.addLayout(self.bottom_layout)
        self.main_layout.addLayout(self.bottom_line_layout)
        self.main_layout.addLayout(self.bottom_right_layout)

    def _init_table(self):
        self.table = QTableWidget(0, 1, self)  # Создаем таблицу с 0 строк и 1 столбцом
        self.table.insertRow(0)
        self.table.setVerticalHeaderLabels([""])
        self.table.setHorizontalHeaderLabels(["Фамилия"])
        self.table.setColumnWidth(0, 120)

        header = self.table.horizontalHeader()
        for i in range(self.table.columnCount()):
            header.setSectionResizeMode(i, QHeaderView.ResizeMode.Interactive)

        # Устанавливаем виджет с ComboBox и кнопкой в ячейку
        self.table.setCellWidget(0, 0, self.cell_widget)

    def _init_cell_double(self):
        # Создаем виджет для ячейки
        self.cell_widget = QWidget()
        self.cell_layout = QHBoxLayout(self.cell_widget)
        self.cell_layout.setContentsMargins(0, 0, 0, 0)  # Убираем отступы

    def _init_group_box(self, groups: list):
        self.group_combo = QComboBox(self)
        self.group_combo.addItems(groups)
        self.cell_layout.addWidget(self.group_combo)
        self.group_combo.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Fixed)
        self.group_combo.setMinimumWidth(80)

    def _init_create_group_btn(self):
        self.create_group_button = QPushButton("➕", self)
        self.create_group_button.setMaximumWidth(30)
        self.cell_layout.addWidget(self.create_group_button)

    def _init_header(self):
        self.top_layout = QHBoxLayout()

        self.exercise_combobox = QComboBox(self)
        self.exercise_combobox.addItems(self.exercises)
        self.top_layout.addWidget(self.exercise_combobox)

        self.term_combobox = QComboBox(self)
        self.top_layout.addWidget(self.term_combobox)

        self.add_exercise_button = QPushButton("Добавить упражнение", self)
        self.top_layout.addWidget(self.add_exercise_button)

        self.remove_exercise_button = QPushButton("Удалить упражнение")
        self.remove_exercise_button.setEnabled(False)
        self.top_layout.addWidget(self.remove_exercise_button)

    def _init_footer(self):
        # Нижняя панель с кнопкой "Добавипть ученика"
        self.bottom_layout = QHBoxLayout()

        # Нижняя панель для отображения информации о ячейке
        self.bottom_line_layout = QHBoxLayout()
        self.result_label = QLabel()
        self.bottom_line_layout.addWidget(self.result_label)
        self.result_label.setMaximumWidth(150)

        # Кнопка "Сохранить"
        self.bottom_right_layout = QHBoxLayout()
        self.save_button = QPushButton("Сохранить", self)
        self.bottom_right_layout.addWidget(self.save_button)
        self.save_button.setMaximumWidth(150)
