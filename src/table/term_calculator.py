from datetime import date
import logging


def get_term_list(group_name: str) -> list:
    year = int(group_name.split('-')[0])
    current_date = [date.today().year, date.today().month]
    course = current_date[0] - year + 1 if current_date[1] >= 9 else current_date[0] - year
    curr_term = course * 2 if 2 <= current_date[1] <= 9 else course * 2 - 1
    terms = [str(i) for i in range(1, curr_term + 1)]
    return terms


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logging.debug(get_term_list('2022-7-3-4'))
