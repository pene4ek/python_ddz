import logging

from src.table.table_window_ui import TableWindowUI
from PyQt6.QtWidgets import QTableWidgetItem, QApplication, QLabel
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QShortcut, QKeySequence
from src.start_window.create_group.group_name_converter import GroupNameConverter
from src.parce_excel.parce_normative import get_user_score, get_exercise_num
from src.error_window.error_base import ErrorDialog
from src.start_window.create_group.create_group_dialog import CreateGroupDialog
from src.table.term_calculator import get_term_list
import sqlite3
import sys
from src.table.table_db_handler import TableDbHandler


class TableWindow:
    def __init__(self, current_group: str, teacher_data: list, connection: sqlite3.Connection):
        self.connection = connection
        self.cursor = self.connection.cursor()
        self.teacher_data = teacher_data

        self._initialize_fields()

        self.table_db_handler = TableDbHandler(self)
        self.exercises = self.table_db_handler.exercises

        self.table_db_handler.load_from_db()

        self.ui = TableWindowUI(self.exercises, self.table_db_handler.groups_base)

        self._subscribe()

        start_index = self.ui.group_combo.findText(current_group)
        self.ui.group_combo.setCurrentIndex(start_index)
        self.ui.group_combo.setCurrentText(current_group)
        self._display_group(start_index)

    def _initialize_fields(self):
        self.current_index_group = -1
        self.num_norms = []  # номера нормативов в таблице
        self.name_norms = []  # имена нормативов в таблице

        self.stud_id = []  # текущие айдишники студентов
        self.stud_names = []  # имена студентов из бд
        self.terms = []  # список семестров для группы

        # Устанавливаем ограничения на кол-во упражнений
        self.MAX_EXERCISES = 5
        self.added_exercises = 0
        self.current_column_count = 0
        self.error_msg = None

    # region subscribe_events
    def _subscribe(self):
        self._subscribe_input()
        self._subscribe_btn()
        self.ui.table.selectionModel().selectionChanged.connect(self.on_selection_changed)

    def _subscribe_input(self):
        shortcut = QShortcut(QKeySequence(Qt.Key.Key_Space), self.ui)
        shortcut.activated.connect(self.on_space_pressed)

    def on_space_pressed(self):
        pass

    def _subscribe_btn(self):
        self.ui.create_group_button.clicked.connect(self._create_group)  # Подключаем обработчик
        self.ui.add_exercise_button.clicked.connect(self._add_exercise_column)
        self.ui.remove_exercise_button.clicked.connect(self._remove_exercise_column)
        self.ui.group_combo.currentIndexChanged.connect(self._display_group)
        self.ui.table.itemChanged.connect(self.update_total)  # Подключаем обработчик изменения ячеек
        self.ui.table.cellClicked.connect(self.on_cell_clicked)
        self.ui.table.cellChanged.connect(self._update_student_name)
        self.ui.save_button.clicked.connect(self.table_db_handler.save_table)
        self.ui.term_combobox.currentIndexChanged.connect(self.table_db_handler.switch_group_data)

    def on_cell_clicked(self, row, column):
        if column == self.ui.table.columnCount() - 1:
            return

        if column > 0 and row == 0:
            self.ui.remove_exercise_button.setEnabled(True)
        else:
            self.ui.remove_exercise_button.setEnabled(False)

        if row == 0 or column == 0:
            self.ui.result_label.setText('')
            return

        item_text = self.ui.table.item(row, column).text()
        if item_text == '':
            self.ui.result_label.setText('')
            return

        self._show_res_stat(column, item_text)

    def _create_group(self):
        CreateGroupDialog(self.connection, self.teacher_data)

    # endregion

    # region add
    def add_last_row(self):
        last_stud = self.ui.table.item(self.ui.table.rowCount() - 1, 0)
        if not (last_stud is None):
            if last_stud != '':
                self._add_student_row()
        elif not (self.ui.table.cellWidget(self.ui.table.rowCount() - 1, 0) is None):
            self._add_student_row()

    def _add_student_row(self):
        self.ui.table.blockSignals(True)
        """Добавляет новую строку для ввода фамилии."""
        row_count = self.ui.table.rowCount()
        self.ui.table.insertRow(row_count)
        row_count = self.ui.table.rowCount()
        for c in range(1, self.ui.table.columnCount()):
            item = QTableWidgetItem("")
            self.ui.table.setItem(row_count - 1, c, item)
            item.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
            logging.debug(f'_add_stud_row {row_count - 1} {c} row_count:{self.ui.table.rowCount()}')

        if self.added_exercises >= 3:
            label = QLabel()
            label.setAlignment(Qt.AlignmentFlag.AlignCenter)
            self.ui.table.setCellWidget(row_count - 1, self.ui.table.columnCount() - 1, label)

        self.ui.table.setVerticalHeaderItem(row_count - 1, QTableWidgetItem(str(row_count - 1)))
        self.ui.table.blockSignals(False)

    def _add_exercise_column(self):
        if self.added_exercises >= self.MAX_EXERCISES:
            logging.debug("Достигнуто максимальное количество упражнений (5).")
            return
        """Добавляет три столбца для нового упражнения с соответствующими названиями."""
        exercise_name = self.ui.exercise_combobox.currentText()

        if not self.add_triple_column(exercise_name):
            return
        self.add_total_column()

    def add_triple_column(self, exercise_name: str):
        num = get_exercise_num(exercise_name)
        if num in self.num_norms:
            self.error_msg = ErrorDialog('Этот норматив уже добавлен')
            self.error_msg.show()
            return False

        self.current_column_count = self.ui.table.columnCount() - 1 if self.added_exercises >= 3 \
            else self.ui.table.columnCount()

        self._add_triple_header()
        self._add_empty_sub_columns()

        self.num_norms.append(num)
        self.name_norms.append(exercise_name)
        header_label = QLabel(exercise_name)
        header_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.ui.table.setSpan(0, self.current_column_count, 1, 3)  # Объединяем 1 строку и 3 столбца
        self.ui.table.setCellWidget(0, self.current_column_count, header_label)

        self.added_exercises += 1
        return True

    def _add_triple_header(self):
        '''
        Создаем три столбца с названиями "Попытка 1", "Попытка 2", "Попытка 3"
        '''
        for i in range(1, 4):
            self.ui.table.insertColumn(self.current_column_count + i - 1)
            header_text = f"Попытка {i}"
            self.ui.table.setHorizontalHeaderItem(
                self.current_column_count + i - 1, QTableWidgetItem(header_text)
            )

    def _add_empty_sub_columns(self):
        '''
        заполняет новые строки пустыми символами
        '''
        for row in range(1, self.ui.table.rowCount()):
            for attempt in range(3):
                item = QTableWidgetItem("")
                item.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
                self.ui.table.blockSignals(True)
                self.ui.table.setItem(row, self.current_column_count + attempt, item)
                self.ui.table.blockSignals(False)

    def add_total_column(self):
        if self.added_exercises == 3:
            columns = self.ui.table.columnCount()
            self.ui.table.insertColumn(columns)
            columns += 1
            self.ui.table.setHorizontalHeaderItem(
                columns - 1, QTableWidgetItem("Итог")
            )
            for r in range(1, self.ui.table.rowCount()):
                label = QLabel()
                label.setAlignment(Qt.AlignmentFlag.AlignCenter)
                self.ui.table.setCellWidget(r, columns - 1, label)
            self._recalculate_total_result()

    # endregion

    # region remove
    def remove_all_column(self):
        for column in range(self.ui.table.columnCount() - 1, 0, -1):  # удаление содержимого
            self.ui.table.removeColumn(column)

    def _remove_exercise_column(self):
        current_column = self.ui.table.currentColumn()
        logging.debug(current_column)
        exercise_name = self.ui.table.cellWidget(0, current_column).text()
        if self.added_exercises >= 1:
            # Получаем текущий индекс колонки

            # Удаляем 3 столбца, связанные с упражнением
            for i in range(3):
                self.ui.table.removeColumn(current_column)

        self.ui.remove_exercise_button.setEnabled(False)

        self._remove_triple_column(exercise_name)
        self._remove_total_column()
        self.added_exercises -= 1
        if self.added_exercises >= 3:
            self._recalculate_total_result()

    def _remove_triple_column(self, exercise_name: str):
        num = get_exercise_num(exercise_name)
        self.num_norms.remove(num)
        self.name_norms.remove(exercise_name)

    def _remove_total_column(self):
        if self.added_exercises == 3:
            columns = self.ui.table.columnCount()
            self.ui.table.removeColumn(columns - 1)

    # endregion

    # region updates
    def update_total(self, item):
        '''
        обновление итогового результата в строке, в которой находится item
        '''
        if not self._validate_input(item):
            return
        if self.added_exercises < 3:
            return
        self._update_total_score(item)

    def _update_student_name(self, row: int, column: int):
        '''
        изменение имени студента
        в случае неправильного формата - оставляет прежнее, либо очищает, если не было ничего до
        '''
        item = self.ui.table.item(row, column)
        if column != 0 and row > 0:
            return
        if len(item.text().split()) != 3:
            e = ErrorDialog("Непрвильнай формат фио слушателя")
            e.exec()
            self.ui.table.blockSignals(True)
            item.setText(self.stud_names[row - 1] if len(self.stud_names) >= row else '')
            self.ui.table.blockSignals(False)
            return

        self.add_last_row()

    def reset_table_data(self):
        self.num_norms = []
        self.name_norms = []
        self.added_exercises = 0

    def _display_group(self, index: int):
        '''
        меняет содержимое таблицы
        '''
        if self.current_index_group == index:
            return

        self.current_index_group = index
        self.current_group = GroupNameConverter.group_to_db(self.ui.group_combo.itemText(index))
        self._remake_term_box()

        self.table_db_handler.switch_group_data(index)

    def _recalculate_total_result(self):
        rows = self.ui.table.rowCount()
        columns = self.ui.table.columnCount()
        for r in range(1, rows):
            for c in range(1, columns - 1):
                item = self.ui.table.item(r, c)
                if item.text() != '':
                    self.update_total(item)
                    break

    def _update_total_score(self, item):
        total_column = self.ui.table.columnCount() - 1
        if item.column() != total_column:
            exercise_scores = self._get_exercise_scores(item.row())
            if exercise_scores:
                best_scores = self._get_best_scores(exercise_scores)
                total_score = sum(best_scores)
                self._set_total_score(item.row(), total_score)

    # endregion

    # region calculate_total
    def _get_exercise_scores(self, row):
        exercise_scores = []
        total_column = self.ui.table.columnCount() - 1
        for col in range(1, total_column):
            item = self.ui.table.item(row, col)
            v = item.text()
            if v != '':
                res_column = self._get_attempt_column(col)
                exercise_name = self.ui.table.cellWidget(0, res_column).text()
                score = get_user_score(exercise_name, v)
                exercise_scores.append(score)
            else:
                exercise_scores.append(0)
        return exercise_scores

    def _get_best_scores(self, exercise_scores):
        best_scores = []
        for i in range(0, len(exercise_scores), 3):
            score = exercise_scores[i:i + 3]
            best_scores.append(max(score))
        return best_scores

    def _set_total_score(self, row, total_score):
        total_column = self.ui.table.columnCount() - 1
        cell_widget = self.ui.table.cellWidget(row, total_column)
        if cell_widget is not None:
            cell_widget.setText(str(total_score))
        else:
            logging.error(f"No cell widget found at row {row}, column {total_column}")

    # endregion
    def on_selection_changed(self, selected, deselected):
        if not selected.indexes():
            self.ui.result_label.setText('')

    def _show_res_stat(self, column: int, item_text: str):
        '''
        отображение конвертации результата в балл, при клике на ячейку
        '''
        col = self._get_attempt_column(column)
        exercise_name = self.ui.table.cellWidget(0, col).text()
        score = get_user_score(exercise_name, item_text)
        result_text = f"Результат: {item_text}\nКол-во баллов: {score}"
        logging.debug(result_text)
        self.ui.result_label.setText(result_text)

    def _validate_input(self, item):
        '''
        корректность ввода в ячейке с результатом
        '''
        if item.text() == '':
            return True
        if self.ui.table.item(item.row(), 0) is None:
            self.ui.table.blockSignals(True)
            item.setText('')
            e = ErrorDialog('ФИО слушателя не заполнено')
            e.exec()
            self.ui.table.blockSignals(False)
            return

        row = item.row()
        column = item.column()
        if not self._check_prev_results(row, column, item):
            return False

        if not item.text().isdigit():
            try:
                if int(item.text()) < 0:
                    item.setText('')
                    return False
            except ValueError:
                pass

        if not item.text().isdigit():
            try:
                float(item.text())
            except ValueError:
                if row >= 1 and column >= 1:
                    item.setText('')
                return False

        return True

    def _check_prev_results(self, row, column, item) -> bool:
        '''
        проверка предыдущих колонок на заполненность
        '''
        start_col = self._get_attempt_column(column)
        logging.debug(f'f_c: {start_col}  c: {column}')
        if column > start_col:
            while column > start_col:
                if self.ui.table.item(row, start_col) is None:
                    return False
                if self.ui.table.item(row, start_col).text() == '':
                    item.setText('')
                    e = ErrorDialog(f'Попытка: {(item.column() - 1) % 3} не заполнена!')
                    e.exec()
                    return False
                start_col += 1
        return True

    def _get_attempt_column(self, column: int) -> int:
        '''
        возвращает номер колонки первой попытки в нормативе
        '''
        if column % 3 == 0:
            column -= 2
        elif column % 3 == 2:
            column -= 1
        return column

    def _remake_term_box(self):
        '''
        изменение списка семестров
        '''
        self.ui.term_combobox.blockSignals(True)
        self.ui.term_combobox.clear()
        self.terms = get_term_list(self.current_group)
        self.ui.term_combobox.addItems(self.terms)
        self.ui.term_combobox.setCurrentIndex(len(self.terms) - 1)
        self.ui.term_combobox.blockSignals(False)

    def show(self):
        self.ui.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    connection = sqlite3.connect("../../data/data_bases/7324.db")
    window = TableWindow('2022-7-3-4', ['Иванов', 'Иван', 'Иваныч'], connection)
    window.show()
    sys.exit(app.exec())
