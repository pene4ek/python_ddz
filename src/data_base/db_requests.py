import logging
import sqlite3


def create_new_db(file_name: str, teacher_data: list):
    '''
    создание файла с новой бд и запрос для создания пустой бд
    :param file_name:
    :param teacher_data:
    :return:
    '''
    path = '../data/data_bases/' + file_name
    connection = sqlite3.connect(path)
    cursor = connection.cursor()

    with open('../data/requests/create_request.txt', 'r', encoding='utf-8') as file:
        request = file.readlines()
        request = ''.join(request)
        request = request.split(';')
        for r in request:
            cursor.execute(r)
        insert_teacher(teacher_data, cursor)
        connection.commit()
        connection.close()
        print('База данных создана')


def insert_teacher(teacher_data: list, cursor: sqlite3.Cursor):
    '''
    вставка учителя в бд
    :param teacher_data:
    :param cursor:
    :return:
    '''
    sql = "INSERT INTO Teachers (name, surname, patronymic) VALUES (?, ?, ?)"
    cursor.execute(sql, teacher_data)


def get_teacher_id(teacher_data: list, cursor: sqlite3.Cursor):
    '''
    получения id препода по его фио
    :param teacher_data: 0 - Фамилия, 1 - Имя, 2 - Отчество
    :param cursor:
    :return:
    '''
    get_id_sql = 'SELECT teacher_id FROM Teachers WHERE name = ? AND surname = ? AND patronymic = ?'
    cursor.execute(get_id_sql, (teacher_data[0], teacher_data[1], teacher_data[2]))

    id = cursor.fetchone()[0]
    if id == 0 or id is None:
        return -1
    return int(id)


def get_teacher_groups(teacher_data: list, cursor: sqlite3.Cursor):
    '''
    Возвращает список всех групп для данного препода
    :param teacher_data:
    :param cursor:
    :return:
    '''
    teacher_id = get_teacher_id(teacher_data, cursor)
    cursor.execute("SELECT group_number FROM Groups WHERE teacher_id = ?", (teacher_id,))
    result = cursor.fetchall()
    groups = [x[0] for x in result]
    return groups


def insert_group(group: str, teacher_data: list, cursor: sqlite3.Cursor):
    sql_check = "SELECT group_number FROM Groups WHERE teacher_id == ?"
    t_i = get_teacher_id(teacher_data, cursor)
    result = cursor.execute(sql_check, (str(t_i)))
    all_groups = result.fetchall()
    all_groups = [x[0] for x in all_groups]

    if group in all_groups:
        return -1

    sql = "INSERT INTO Groups (group_number, teacher_id) VALUES (?, ?)"
    cursor.execute(sql, (group, get_teacher_id(teacher_data, cursor)))


def get_group_info(group_num: str, cursor: sqlite3.Cursor):
    cursor.execute("SELECT name, surname, patronymic  FROM Students WHERE group_number = ?", (group_num,))
    result = cursor.fetchall()
    names = [' '.join(x) for x in result]
    cursor.execute('SELECT student_id FROM Students WHERE group_number = ?', (group_num,))
    stud_id = cursor.fetchall()
    stud_id = [x[0] for x in stud_id]
    return names, stud_id


def get_stud_count(group_num: str, cursor: sqlite3.Cursor):
    sql = 'SELECT COUNT(student_id) FROM Students'
    cursor.execute(sql)
    count = cursor.fetchone()[0]
    return count


def insert_stud(stud_data: list, group_num: str, stud_id: int, cursor: sqlite3.Cursor):
    sql = 'INSERT INTO Students (name, surname, patronymic, group_number, student_id) VALUES (?, ?, ?, ?, ?)'
    cursor.execute(sql, (stud_data[0], stud_data[1], stud_data[2], group_num, stud_id,))


def update_stud_name(stud_data: list, stud_id: int, cursor: sqlite3.Cursor):
    sql = 'UPDATE Students SET name = ?, surname = ?, patronymic = ? WHERE student_id = ?'
    cursor.execute(sql, (stud_data[0], stud_data[1], stud_data[2], stud_id));


def get_norms_info(cursor: sqlite3.Cursor):
    sql = 'SELECT norm_name, norm_number FROM Norms'
    cursor.execute(sql)
    res = cursor.fetchall()
    name = [x[0] for x in res]
    num = [x[1] for x in res]
    return name, num


def insert_norm(name: str, num: int, cursor: sqlite3.Cursor):
    sql = 'INSERT INTO Norms (norm_name, norm_number) VALUES (?, ?)'
    cursor.execute(sql, (name, num,))


def insert_result(term: int, result: float, result_score: int, attempt: int, norm_number: int, student_id: int,
                  cursor: sqlite3.Cursor):
    sql = "INSERT INTO Results (term, result, result_score, attempt_number, norm_number, student_id) VALUES (?, ?, ?, ?, ?, ?)"
    cursor.execute(sql, (term, result, result_score, attempt, norm_number, student_id,))


def check_res_db(term: int, stud_id: id, attempt: int, norm_number: int, cursor: sqlite3.Cursor) -> bool:
    sql = "SELECT result_id FROM Results WHERE term = ? AND attempt_number = ? AND norm_number = ? AND student_id = ?"
    cursor.execute(sql, (term, attempt, norm_number, stud_id))
    res = cursor.fetchone()
    return not (res is None)

def get_exercises(term: int, group_name: str, cursor: sqlite3.Cursor):
    sql = 'SELECT norm_name FROM Norms WHERE norm_number IN (SELECT norm_number FROM Results WHERE student_id IN (SELECT student_id FROM Students WHERE group_number = ?) AND term = ?)'
    cursor.execute(sql, (group_name, term,))
    res = cursor.fetchall()
    names = [x[0] for x in res]
    return names

def get_results(term: int, group_name: str, cursor: sqlite3.Cursor):
    sql = "SELECT result, attempt_number, student_id, norm_number FROM Results WHERE term = ? AND student_id IN (SELECT student_id FROM Students WHERE group_number = ?)"
    cursor.execute(sql, (term, group_name,))
    return cursor.fetchall()