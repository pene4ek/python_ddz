import sqlite3
from src.data_base.db_requests import get_teacher_groups
from src.start_window.table_with_group_list import TableGroup


def open_db(file_name: str, teacher_data: list) -> sqlite3.Connection:
    '''
    открытие бд для данного препода с проверкой на его наличие в бд
    :param file_name:
    :param teacher_data:
    :return:
    '''
    connection = sqlite3.connect(file_name)
    cursor = connection.cursor()
    res = check_teacher_on_db(teacher_data, cursor, file_name)
    connection.commit()
    return connection, cursor, res


def check_teacher_on_db(teacher_data: list, cursor: sqlite3.Cursor, path: str):
    '''
    проверка наличия препода в бд
    :param teacher_data:
    :param cursor:
    :return:
    '''
    cursor.execute("SELECT COUNT(*) FROM Teachers WHERE name = ? AND surname = ? AND patronymic = ?",
                   (teacher_data[0], teacher_data[1], teacher_data[2]))

    result = cursor.fetchone()[0]

    if result == 0:
        return False
    else:
        return True
