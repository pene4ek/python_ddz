import sys
from PyQt6.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QComboBox, QPushButton, QHBoxLayout, QMenuBar, QMainWindow
from src.widget_styles import *
from PyQt6.QtCore import Qt

class StartWindowUI(QMainWindow):
    '''
    Визуальная часть стартового окна
    '''

    def __init__(self):
        super().__init__()
        self.central_widget = QWidget()

        self._init_ui()
        self.setCentralWidget(self.central_widget)

    def _init_ui(self):
        self.setWindowTitle("Start Window")
        self.setMinimumSize(WINDOW_WIDTH, WINDOW_HEIGHT)
        self.main_layout = QVBoxLayout()

        self._init_menu_bar()
        self._init_body()

        self.main_layout.setMenuBar(self.menu_bar)
        self.main_layout.addLayout(self.top_layout)
        self.main_layout.addLayout(self.teacher_layout)

        self.main_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.central_widget.setLayout(self.main_layout)

    def _init_body(self):
        self._init_create_db_layout()
        self._init_select_db_layout()
        self._init_top_layout()
        self._init_select_teacher_layout()

    def _init_menu_bar(self):
        self.menu_bar = QMenuBar(self)
        self.info_action = self.menu_bar.addAction('Info')

    def _init_create_db_layout(self):
        self.create_layout = QVBoxLayout()
        self.create_label = QLabel('Create new DB')
        self.create_btn = QPushButton("Create DB")
        self.create_btn.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)

        self.create_layout.addWidget(self.create_label, alignment=Qt.AlignmentFlag.AlignCenter)
        self.create_layout.addWidget(self.create_btn, alignment=Qt.AlignmentFlag.AlignCenter)

    def _init_select_db_layout(self):
        self.select_layout = QVBoxLayout()
        self.select_label = QLabel('Select exist DB')
        self.select_btn = QPushButton("Select DB")
        self.select_btn.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)

        self.select_layout.addWidget(self.select_label, alignment=Qt.AlignmentFlag.AlignCenter)
        self.select_layout.addWidget(self.select_btn, alignment=Qt.AlignmentFlag.AlignCenter)

    def _init_top_layout(self):
        self.top_layout = QHBoxLayout()
        self.top_layout.addLayout(self.create_layout)
        self.top_layout.addLayout(self.select_layout)
        self.top_layout.setContentsMargins(0, 0, 0, 50)

    def _init_select_teacher_layout(self):
        self.teacher_layout = QVBoxLayout()
        self.teacher_label = QLabel('Select Teacher')
        self.teacher_btn = QPushButton('Select')
        self.teacher_btn.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)

        self.combo_box_teacher = QComboBox()
        self.combo_box_teacher.setEditable(True)
        self.combo_box_teacher.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)

        self.teacher_layout.addWidget(self.teacher_label, alignment=Qt.AlignmentFlag.AlignCenter)
        self.teacher_layout.addWidget(self.combo_box_teacher, alignment=Qt.AlignmentFlag.AlignCenter)
        self.teacher_layout.addWidget(self.teacher_btn, alignment=Qt.AlignmentFlag.AlignCenter)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    mainWindow = StartWindowUI()
    mainWindow.show()

    sys.exit(app.exec())
