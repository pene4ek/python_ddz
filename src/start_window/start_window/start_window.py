from PyQt6.QtWidgets import QApplication, QFileDialog, QWidget, QVBoxLayout
import sys
from src.start_window.info_dialog import InfoWindow
from src.start_window.create_db_widget import CreateDbWidget
from src.data_base.db_utils import open_db
from src.error_window.error_base import ErrorDialog
from src.start_window.start_window.ui_start_window import StartWindowUI
from src.events.EventBus import EventBus
import logging


class StartWindow:
    '''
    Логика работы стартового окна
    '''

    def __init__(self):
        super().__init__()
        self.ui = StartWindowUI()
        self._btn_disabled(True)
        self._teacher_box_empty()
        self._subscribes()
        self._read_teachers()
        self.connection = None
        self.teacher_name = ''

        self.create_group_dialog = None

    def _add_teacher_to_file(self, teacher_name: str):
        '''
        Добавление преподавателя в файл для combobox с выбором
        :param teacher_name:
        :return:
        '''
        with open('../data/users.txt', 'a', encoding='utf-8') as file:
            s = '\n' + teacher_name
            file.write(s)

    def _read_teachers(self):
        with open('../data/users.txt', 'r', encoding='utf-8') as file:
            teachers = file.read().splitlines()
            self.ui.combo_box_teacher.addItems(teachers)
            self.ui.combo_box_teacher.setCurrentText('')

    def _subscribes(self):
        self.ui.info_action.triggered.connect(self._info_dialog)
        self.ui.select_btn.clicked.connect(self._select_file_dialog)
        self.ui.teacher_btn.clicked.connect(self._add_teacher)
        self.ui.create_btn.clicked.connect(self._create_file_dialog)

    def _add_teacher(self):
        '''
        добавление препода в combobox
        :return:
        '''
        if self._teacher_box_empty():
            self._btn_disabled(True)
            return

        self.teacher_name = self.ui.combo_box_teacher.currentText()
        if not self._correct_teacher_name():
            return

        self._btn_disabled(False)
        if self.teacher_name not in [self.ui.combo_box_teacher.itemText(i) for i in
                                     range(self.ui.combo_box_teacher.count())]:
            self.ui.combo_box_teacher.addItem(self.teacher_name)
            self._add_teacher_to_file(self.teacher_name)

    def _correct_teacher_name(self) -> bool:
        '''
        проверка корректности введеного фио препода
        :return:
        '''
        name = self.teacher_name.split(' ')
        if len(name) == 3:
            return True

        error = ErrorDialog('Формат ФИО неверный\n\nФормат ввода:\nФамилия Имя Отчество')
        error.exec()
        return False

    def _btn_disabled(self, disabled: bool):
        '''
        отключение/включение кнопок для даленейшего пользования прогой
        включаются в случае правильно заполненного фио
        :param disabled:
        '''
        self.ui.create_btn.setDisabled(disabled)
        self.ui.select_btn.setDisabled(disabled)

    def _teacher_box_empty(self) -> bool:
        return '' == self.ui.combo_box_teacher.currentText()

    def _create_file_dialog(self):
        '''
        Открывает окно с созданием новой бд
        :return:
        '''
        create_group_dialog = CreateDbWidget(self.teacher_name.split(' '))
        self.switch_widget(create_group_dialog)

    def switch_widget(self, widget: QWidget):
        self.ui.setCentralWidget(widget)
        self.ui.show()

    def _info_dialog(self):
        '''
        открывает окно с инфо
        :return:
        '''
        InfoWindow()

    def _select_file_dialog(self):
        '''
        отрывает окно с выбором бд и получает выбранный файл
        :return:
        '''
        file_name, _ = QFileDialog.getOpenFileName(self.ui, caption="Выберите файл", filter='*.db',
                                                   directory='../data/data_bases/')
        if file_name:
            EventBus().on_show_group_list.invoke(file_name, self.teacher_name.split(' '))
            print(f'Выбранный файл: {file_name}')

    def show(self):
        '''
        открытие окна
        :return:
        '''
        self.ui.show()

    def close(self):
        '''
        закрытие окна
        :return:
        '''
        self.ui.close()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    app = QApplication(sys.argv)

    StartWindow = StartWindow()
    StartWindow.show()

    sys.exit(app.exec())
