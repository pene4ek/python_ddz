'''
Преобразование имен групп
'''
from datetime import datetime

institute_map = {
    '7': '7',
    '1': 'K-1',  # K латинская
    '2': '2',
    '4': '4',
    'фпм': '7',
    'фиб': '7',
    'фст': '7',
    'отф': '7',
    'крф': '1',
    'сф': '2',
    'фия': '4',
}

faculty_map = {
    'фпм': '1',
    'фиб': '3',
    'фст': '2',
    'отф': '4',
}


class GroupNameConverter:
    '''
    Содержит в себе методы по преобразованию одного имени группы в другое
    Содержит метод по преобразованию данных из окна при создании группы в вид для бд

    2022-7-3-4 год, институт, факультет, группа, факультет = 0 в случае, если его "нет"
    '''

    @staticmethod
    def db_to_group(group_db: str, dt: datetime = datetime.now()) -> str:
        '''
        На вход получает группу вида 2022-7-3-4 и преобразует ее в 7324
        '''
        group_info = group_db.split('-')

        course = dt.year - int(group_info[0]) + 1 if dt.month >= 9 else dt.year - int(group_info[0])
        faculty = '' if group_info[2] == '0' else group_info[2]

        group_name = f'{institute_map[group_info[1]]}{faculty}{str(course)}{group_info[-1]}'
        return group_name

    @staticmethod
    def group_to_db(group_name: str, dt: datetime = datetime.now()) -> str:
        '''
            На вход получает группу вида 7324 и преобразует ее в 2022-7-3-4
        '''
        institute_code = ''

        course = int(group_name[-2])
        group_number = group_name[-1]
        year_formed = dt.year - course + 1 if dt.month >= 9 else dt.year - course
        faculty_code = group_name[-3] if len(group_name) == 4 else '0'

        i_code = group_name[:-3] if len(group_name) == 4 else group_name[:-2]
        for key, value in institute_map.items():
            if value == i_code:
                institute_code = key
                break

        group_db = f"{year_formed}-{institute_code}-{faculty_code}-{group_number}"
        return group_db

    @staticmethod
    def create_group_name(course: int, faculty: str, group: str, dt: datetime = datetime.now()) -> str:
        '''
        Метод который создает имя группы вида 2022-7-3-4 из данных полученных из окна создания
        '''
        year_formed = dt.year - course + 1 if dt.month >= 9 else dt.year - course
        faculty_code = '0'
        for k, v in faculty_map.items():
            if k == faculty.lower():
                faculty_code = v
                break
            else:
                faculty_code = '0'
        institute_code = institute_map[faculty.lower()]

        group_db = f'{year_formed}-{institute_code}-{faculty_code}-{group}'
        return group_db

