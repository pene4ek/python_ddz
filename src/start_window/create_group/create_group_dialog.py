from PyQt6.QtWidgets import QDialog, QVBoxLayout
from src.start_window.create_group.create_group_widget import CreateGroupWidget
import sqlite3


class CreateGroupDialog(QDialog):
    def __init__(self, connection: sqlite3.Connection, teacher_data: list, parent=None):
        super().__init__(parent)
        main_layout = QVBoxLayout()

        widget = CreateGroupWidget(connection, teacher_data, self)
        widget.create_btn.clicked.connect(self.close)

        main_layout.addWidget(widget)
        self.setLayout(main_layout)
        self.exec()
