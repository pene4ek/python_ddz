'''
Всплывающие окно с созданием новой группы
'''
import sqlite3

from PyQt6.QtWidgets import QApplication, QWidget, QVBoxLayout, QComboBox, QPushButton, QLineEdit, QDialog
from PyQt6.QtGui import QIntValidator
from PyQt6.QtCore import Qt
import sys

from src.widget_styles import *
import logging
from src.events.EventBus import EventBus
from src.start_window.create_group.group_name_converter import GroupNameConverter
from src.data_base.db_requests import insert_group
from src.error_window.error_base import ErrorDialog


class CreateGroupWidget(QWidget):
    def __init__(self, connection: sqlite3.Connection, teacher_data: list, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.teacher_data = teacher_data
        self.connection = connection
        self.setWindowTitle('Создание новой группы')
        self._init_ui()
        self._subscribe()
        self.error = None

    def _init_ui(self):
        self.setMinimumSize(SMALL_WINDOW_WIDTH, SMALL_WINDOW_HEIGHT)
        self.main_layout = QVBoxLayout()
        self.validator = QIntValidator()

        self._init_create_btn()
        self._init_group_input()
        self._init_corse_input()
        self._init_select_fac_box()

        self._add_widgets()
        self.main_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.setLayout(self.main_layout)

    def _check_fields(self):
        '''
        Проверяет правильно ли заполнены поля
        :return: выходит если поля заполнены неверно
        '''
        course = self.course_input.text()
        group = self.group_input.text()
        if course == '' or group == '':
            self.create_btn.setDisabled(True)
            logging.debug(f'Can create group: False')
            return
        correct_course = course != '' and (1 <= int(course) <= 5)
        correct_group = 6 >= int(group) >= 1
        correct = self.select_fac_box.currentText() != '' and correct_group and correct_course
        logging.debug(f'Can create group: {correct}')
        self.create_btn.setDisabled(not correct)

    def _subscribe(self):
        '''
        подписки на события
        '''
        self.course_input.textEdited.connect(self._check_fields)
        self.group_input.textEdited.connect(self._check_fields)
        self.select_fac_box.currentIndexChanged.connect(self._check_fields)
        self.create_btn.clicked.connect(self._create_group)

    def _create_group(self):
        '''
        создание имени новой группы из параметров, введенных в окне
        :return:
        '''
        course = int(self.course_input.text())
        group = self.group_input.text()
        fac = self.select_fac_box.currentText()
        group_name = GroupNameConverter.create_group_name(course=course, group=group, faculty=fac)
        v = insert_group(group_name, self.teacher_data, self.connection.cursor())

        if v == -1:
            self.error = ErrorDialog('Эта группа уже существует')
            if not (self.parent is None):
                self.close()
                self.error.show()
                return
            self.error.show()
            return

        logging.debug(f'Group created: {group_name}, {GroupNameConverter.db_to_group(group_name)}')
        self.connection.commit()
        EventBus().on_open_table.invoke(GroupNameConverter.db_to_group(group_name))

    def _add_widgets(self):
        self.main_layout.addWidget(self.select_fac_box)
        self.main_layout.addWidget(self.course_input)
        self.main_layout.addWidget(self.group_input)
        self.main_layout.addWidget(self.create_btn)

    def _init_corse_input(self):
        self.course_input = QLineEdit()
        self.course_input.setPlaceholderText('Введите номер курса')
        self.course_input.setValidator(self.validator)
        self.course_input.setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT)

    def _init_group_input(self):
        self.group_input = QLineEdit()
        self.group_input.setPlaceholderText('Введите номер группы')
        self.group_input.setValidator(self.validator)
        self.group_input.setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT)

    def _init_select_fac_box(self):
        self.select_fac_box = QComboBox()
        self.select_fac_box.setPlaceholderText('Выберите факультет')
        self.select_fac_box.addItems(['ФПМ', "ФИБ", "ФСТ", "ОТФ", "СФ", "КРФ", "ФИЯ"])
        self.select_fac_box.setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT)

    def _init_create_btn(self):
        self.create_btn = QPushButton('Создать')
        self.create_btn.setDisabled(True)
        self.create_btn.setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    app = QApplication(sys.argv)
    w = CreateGroupWidget()
    sys.exit(app.exec())
