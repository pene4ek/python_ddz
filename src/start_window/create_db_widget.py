from PyQt6.QtWidgets import QDialog, QVBoxLayout, QLineEdit, QPushButton, QWidget
from src.widget_styles import *
from src.error_window.error_base import ErrorDialog
import os
from src.data_base.db_requests import create_new_db
from src.events.EventBus import EventBus

class CreateDbWidget(QWidget):
    '''
    Окно для создания нового файла бд
    '''

    def __init__(self, teacher_data: list):
        super().__init__()
        self._init_ui()
        self.create_btn.clicked.connect(self._create_db_file)
        self.teacher_data = teacher_data

    def _init_ui(self):
        self._init_input_field()
        self._init_btn()
        self._set_widgets()
        self.setLayout(self.main_layout)

    def _init_input_field(self):
        self.input_field = QLineEdit()
        self.input_field.setPlaceholderText("Enter text here")
        self.input_field.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)

    def _init_btn(self):
        self.create_btn = QPushButton('Create')
        self.create_btn.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)

    def _set_widgets(self):
        self.main_layout = QVBoxLayout()
        self.main_layout.addWidget(self.input_field)
        self.main_layout.addWidget(self.create_btn)

    def _create_db_file(self):
        file_name = self.input_field.text()
        path = '../data/data_bases/'
        if file_name == '':
            dlg = ErrorDialog('Enter file name')
            dlg.exec()
            return
        file_name += '.db'
        path += file_name

        if os.path.isfile(path):
            dlg = ErrorDialog('Этот файл уже существует')
            dlg.exec()
            return

        with open(path, 'w', encoding='utf-8'):
            create_new_db(file_name, self.teacher_data)

        EventBus().on_create_group_dialog.invoke(path, self.teacher_data)
