import logging

from PyQt6.QtWidgets import QTableWidget, QWidget, QVBoxLayout, QTableWidgetItem, QPushButton
from PyQt6.QtCore import Qt
from src.widget_styles import *
from src.events.EventBus import EventBus
from src.start_window.create_group.group_name_converter import GroupNameConverter
from src.start_window.create_group.create_group_widget import CreateGroupWidget


class TableGroup(QWidget):
    '''
    Таблица, в которой выводятся все группы для данного преподавателя
    '''

    def __init__(self, groups: list):
        super().__init__()
        self.groups = groups
        self._init_ui()

    def _init_ui(self):
        self.setMinimumSize(WINDOW_WIDTH, WINDOW_HEIGHT)
        self.setWindowTitle('Выбор группы')
        self.main_layout = QVBoxLayout()

        self.table = QTableWidget(len(self.groups)+1, 1)
        self.table.setMaximumWidth(BUTTON_WIDTH + 20)
        for i in range(len(self.groups)):
            btn = QPushButton(str(self.groups[i]))
            btn.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)
            btn.clicked.connect(lambda checked, group_num=i: self._group_select(self.groups[group_num]))
            self.table.setCellWidget(i, 0, btn)

        self.btn_new = QPushButton('Create new group')
        self.btn_new.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)
        self.table.setCellWidget(len(self.groups), 0, self.btn_new)

        self.main_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.main_layout.addWidget(self.table)
        self.setLayout(self.main_layout)

    def _open_create_group(self):
        self.close()
        # cd = CreateGroupDialog()
        # cd.show()

    def _group_select(self, group_num: str):
        EventBus().on_open_table.invoke(group_num)
        logging.debug(f'Выбрана группа {group_num}')
