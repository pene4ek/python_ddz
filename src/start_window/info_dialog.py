from PyQt6.QtWidgets import QDialog, QLabel, QVBoxLayout
from PyQt6.QtCore import Qt
from src.widget_styles import *

class InfoWindow(QDialog):
    '''
    Небольшая справка
    '''
    def __init__(self):
        super().__init__()
        self.setMinimumSize(SMALL_WINDOW_WIDTH, SMALL_WINDOW_HEIGHT)
        self.setWindowTitle('Info')

        self.label = QLabel(
            'Создатели:\nЛитвинов Даниил 7324\nАлборов Александр 7324\n\n---------\n\nПод Руководством:\nЯрового Алексей Владимировича')
        self.label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.label)
        main_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.setLayout(main_layout)
        self.exec()
