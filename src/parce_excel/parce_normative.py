import logging

import openpyxl


def get_exercise_num(exercise_name: str):
    excel_file = '../data/points.xlsx'

    # Открываем книгу Excel
    wb = openpyxl.load_workbook(excel_file)
    sheet = wb.active

    # Находим столбец с нужным упражнением
    for col in range(2, sheet.max_column + 1):
        v = sheet.cell(row=1, column=col).value
        v = v.split('\n')

        if v[1] == exercise_name:
            num = v[0].split(' ')[1]
            return num


def get_all_exercise():
    excel_file = '../data/points.xlsx'

    # Открываем книгу Excel
    wb = openpyxl.load_workbook(excel_file)
    sheet = wb.active
    name = []
    num = []

    for col in range(2, sheet.max_column + 1):
        v = sheet.cell(row=1, column=col).value
        v = v.split('\n')
        name.append(v[1])
        num.append(v[0].split(' ')[1])

    return name, num


def get_user_score(exercise_name: str, user_result: str):
    """
    Сравнивает баллы пользователя с баллами из Excel файла и возвращает
    балл, набранный пользователем.

    Args:
        exercise_name (str): Название упражнения (например, "Подтягивания").
        user_result (int): Баллы, указанные пользователем за упражнение.

    Returns:
        int: Балл, соответствующий указанному пользователем, или None,
             если упражнение не найдено.
    """

    # Путь к вашему Excel файлу
    excel_file = '../data/points.xlsx'

    # Открываем книгу Excel
    wb = openpyxl.load_workbook(excel_file)
    sheet = wb.active

    # Находим столбец с нужным упражнением
    exercise_column = None
    for col in range(2, sheet.max_column + 1):
        v = sheet.cell(row=1, column=col).value
        v = v.split('\n')
        if v[1] == exercise_name:
            exercise_column = col
            break

    if exercise_column is None:
        logging.error(f"Упражнение '{exercise_name}' не найдено!")
        return None

    u = sheet.cell(row=97, column=exercise_column).value
    u = u.split(' ')

    user_result = float(user_result)
    max_score = 0
    _result_max = sheet.cell(row=2, column=exercise_column).value

    # подсчет результата, превыщающего 100 баллов
    # если на время - время должно быть меньше
    # если на количество - количество должно быть больше
    if u[1] == 'раз':
        if _result_max < user_result:
            max_score = int(abs(user_result - _result_max) // float((u[0]).replace(',', '.'))) * int(u[3]) + 100
            print(max_score)
            return max_score
    else:
        if _result_max > user_result:
            max_score = int(abs(user_result - _result_max) // float((u[0]).replace(',', '.'))) * int(u[3]) + 100
            print(max_score)
            return max_score

    # Конвертация обычного результата в баллы
    for row in range(2, sheet.max_row):
        _result = sheet.cell(row=row, column=exercise_column).value
        if _result is None:
            continue
        _result = float(_result)
        if u[1] == 'с':
            if _result >= user_result and int(sheet.cell(row=row, column=1).value) > max_score:
                max_score = int(sheet.cell(row=row, column=1).value)
        else:
            if _result == user_result:
                return int(sheet.cell(row=row, column=1).value)

    return max_score

# if __name__ == '__main__':
#     exercise = input()
#     result = float(input())
#
#     user_final_score = get_user_score(exercise, result)
#
#     if user_final_score is not None:
#         print(f"Ваш балл за упражнение '{exercise}': {user_final_score}")
#     else:
#         print(f"Для результата {result} в упражнении '{exercise}' баллы не найдены.")
