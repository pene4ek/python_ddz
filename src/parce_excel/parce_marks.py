import openpyxl


def get_grade(course, scores):
    """
    Вычисляет итоговый балл и оценку по физической культуре.

    Args:
        course (int): Номер курса (1-5).
        scores (list): Список баллов за упражнения.

    Returns:
        str: Строка вида "Итоговый балл(оценка)" или сообщение об ошибке.
    """

    excel_file = '../data/limits.xlsx'
    wb = openpyxl.load_workbook(excel_file)
    sheet = wb.active

    # Получаем нижний порог баллов для курса
    min_score = sheet.cell(row=course + 2, column=2).value

    # Проверяем баллы на порог
    for score in scores:
        if score < min_score:
            return "Ошибка: Один или более баллов ниже допустимого порога!"

    total_score = sum(scores)
    num_exercises = len(scores)

    # Определяем колонки для оценок в зависимости от количества упражнений
    if num_exercises == 3:
        grade_columns = (3, 4, 5)
    elif num_exercises == 4:
        grade_columns = (6, 7, 8)
    elif num_exercises == 5:
        grade_columns = (9, 10, 11)
    else:
        return "Ошибка: Неверное количество упражнений (должно быть от 3 до 5)."

    # Получаем оценку
    for i, col in enumerate(grade_columns):
        grade_threshold = sheet.cell(row=course + 2, column=col).value
        if total_score >= grade_threshold:
            grade = 5 - i
            break
    else:
        grade = 2

    return f"Итоговый балл: {total_score} (Оценка: {grade})"


if __name__ == '__main__':
    course = int(input("Введите номер курса (1-5): "))
    scores = []
    while True:
        score_str = input("Введите балл за упражнение (или Enter для завершения): ")
        if not score_str:
            break
        scores.append(int(score_str))

    result = get_grade(course, scores)
    print(result)
