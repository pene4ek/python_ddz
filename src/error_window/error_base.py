from PyQt6.QtWidgets import QDialog, QVBoxLayout, QLabel
from src.widget_styles import *
from PyQt6.QtCore import Qt


class ErrorDialog(QDialog):
    '''
    Базовое окно с ошибкой
    '''
    def __init__(self, message: str):
        super().__init__()
        self.setWindowTitle(message)
        self.setMinimumSize(ERROR_WIDTH, ERROR_HEIGHT)
        label = QLabel(message)
        self.main_layout = QVBoxLayout()
        self.main_layout.addWidget(label)
        self.main_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.setLayout(self.main_layout)
