from PyQt6.QtWidgets import QDialog, QLabel, QPushButton, QVBoxLayout, QHBoxLayout
from PyQt6.QtCore import Qt
from src.widget_styles import *
from src.data_base.db_requests import insert_teacher
from src.events.EventBus import EventBus
import sqlite3


class TeacherNotExistDialog(QDialog):
    '''
    Окно об ошибке, появляющиеся в случае выбора бд, в которой нет данного преподавателя
    '''

    def __init__(self, teacher_data: list, cursor: sqlite3.Cursor, path: str):
        super().__init__()

        self._init_ui()
        self._subscribe()
        self.teacher_data = teacher_data
        self.cursor = cursor
        self.path = path

        self.exec()

    def _init_ui(self):
        self.main_layout = QVBoxLayout()
        self.setWindowTitle('ФИО нет в бд')
        self.bottom_layout = QHBoxLayout()

        self.btn_yes = QPushButton('Yes')
        self.btn_yes.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)
        self.btn_no = QPushButton('No')
        self.btn_no.setMinimumSize(BUTTON_WIDTH, BUTTON_HEIGHT)

        self.bottom_layout.addWidget(self.btn_yes)
        self.bottom_layout.addWidget(self.btn_no)

        self.text_label = QLabel('Вашего ФИО нет в БД, хотите добавить?')
        self.main_layout.addWidget(self.text_label)
        self.main_layout.addLayout(self.bottom_layout)
        self.main_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.setLayout(self.main_layout)

    def _subscribe(self):
        self.btn_no.clicked.connect(self.close)
        self.btn_yes.clicked.connect(self._add_teacher)

    def _add_teacher(self):
        insert_teacher(teacher_data=self.teacher_data, cursor=self.cursor)
        self.close()
        EventBus().on_teacher_not_exist_yes.invoke(self.teacher_data)

