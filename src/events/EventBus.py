from src.events.Event import Event


class EventBus:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(EventBus, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        if not hasattr(self, 'on_create_group_main'):
            self.on_create_group_main = Event()
        if not hasattr(self, 'on_show_group_list'):
            self.on_show_group_list = Event()
        if not hasattr(self, 'on_open_table'):
            self.on_open_table = Event()
        if not hasattr(self, 'on_create_group_dialog'):
            self.on_create_group_dialog = Event()
        if not hasattr(self, 'on_teacher_not_exist_yes'):
            self.on_teacher_not_exist_yes = Event()
