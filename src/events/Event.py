from typing import Callable


class Event:
    def __init__(self):
        self._arr = []

    def add_listener(self, func: Callable[..., None]):
        self._arr.append(func)

    def invoke(self, *args):
        for func in self._arr:
            func(*args)
