from src.start_window.create_group.group_name_converter import GroupNameConverter
import datetime


# region: db_normal
def test_groub_db_to_normal_iksi():
    dt = datetime.datetime(2024, 5, 22)
    group_db = '2022-7-3-4'
    excepted_group = '7324'

    assert GroupNameConverter.db_to_group(group_db, dt) == excepted_group


def test_groub_db_to_normal_krf():
    dt = datetime.datetime(2024, 5, 22)
    group_db = '2020-1-0-3'
    excepted_group = 'K-143'

    assert GroupNameConverter.db_to_group(group_db, dt) == excepted_group


def test_groub_db_to_normal_сf():
    dt = datetime.datetime(2024, 5, 22)
    group_db = '2021-2-0-1'
    excepted_group = '231'

    assert GroupNameConverter.db_to_group(group_db, dt) == excepted_group


def test_groub_db_to_normal_fia():
    dt = datetime.datetime(2024, 5, 22)
    group_db = '2022-4-0-4'
    excepted_group = '424'

    assert GroupNameConverter.db_to_group(group_db, dt) == excepted_group


# endregion: db_normal

# region: normal_db
def test_normal_to_db_iksi():
    dt = datetime.datetime(2024, 5, 22)
    group = '7324'
    excepted_group = '2022-7-3-4'

    assert GroupNameConverter.group_to_db(group, dt) == excepted_group


def test_normal_to_db_fia():
    dt = datetime.datetime(2024, 5, 22)
    group = '424'
    excepted_group = '2022-4-0-4'
    assert GroupNameConverter.group_to_db(group, dt) == excepted_group


def test_normal_to_db_cf():
    dt = datetime.datetime(2024, 5, 22)
    group = '231'
    excepted_group = '2021-2-0-1'
    assert GroupNameConverter.group_to_db(group, dt) == excepted_group


def test_normal_to_db_krf():
    dt = datetime.datetime(2024, 5, 22)
    group = 'K-143'
    excepted_group = '2020-1-0-3'
    assert GroupNameConverter.group_to_db(group, dt) == excepted_group


# endregion: normal_db

# region: info_db
def test_info_to_db_iksi():
    dt = datetime.datetime(2024, 5, 22)
    course = 1
    faculty = "ФПМ"
    group = '3'
    excepted_group = '2023-7-1-3'

    assert GroupNameConverter.create_group_name(course, faculty, group, dt) == excepted_group


def test_info_to_db_fia():
    dt = datetime.datetime(2024, 5, 22)
    course = 5
    faculty = "ФИЯ"
    group = '4'
    excepted_group = '2019-4-0-4'

    assert GroupNameConverter.create_group_name(course, faculty, group, dt) == excepted_group


def test_info_to_db_cf():
    dt = datetime.datetime(2024, 5, 22)
    course = 2
    faculty = "СФ"
    group = '2'
    excepted_group = '2022-2-0-2'

    assert GroupNameConverter.create_group_name(course, faculty, group, dt) == excepted_group


def test_info_to_db_krf():
    dt = datetime.datetime(2024, 5, 22)
    course = 1
    faculty = "КРФ"
    group = '3'
    excepted_group = '2023-1-0-3'

    assert GroupNameConverter.create_group_name(course, faculty, group, dt) == excepted_group
# endregion: info_db
